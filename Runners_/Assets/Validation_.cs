﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class Validation_ : MonoBehaviour {

    public InputField INPUT_idUsuario;
    public Button ValidacionUsuario;
    public GameObject UI_Login;
    public GameObject Beneficios;
    public GameObject LoguedPanel;
    [Header("Login")]
    public RunnersAppServices RAPPS;   
    public string LoginURL;// = "https://tarjetarunners.com.ar/services/validate";

    //Resultado
    [Header("Panel Resultado")]
    public GameObject PanelResultado;
    public Button Aceptar;
    public Text ValidationResultText;
    string validationTrue = "El usuario se encuentra suscripto";
    string validationFalse = "El usuario no se encuentra suscripto";
    bool validUser;
    string idHolders;
    [Header("Datos usuario Logueado")]
    public Text Activo;// = "public string Activo";
    public Text UserName;
    public Text UserEmail;
    public Text UserId;
    public Text UserType;
    public Text FechaValida;
   

    void Start () {
        INPUT_idUsuario.text = "";
     //   ValidacionUsuario.onClick.AddListener(validarUsuario);
       // Aceptar.onClick.AddListener(closeResultPanel);
    }
	
	// Update is called once per frame
	void Update () {
       
    }

    

    public void validarUsuario()
    {
        Debug.Log("validarUsuario");
        StartCoroutine(RAPPS.downloadUserInfo("https://tarjetarunners.com.ar/services/validate", "FDBnDScEZJ89nPeuUBUMAU0Gmnbeu6yg", "119033345"));
        /*
        Debug.Log("Ingresa en validacion. Resultado: " + INPUT_idUsuario.text);
        PanelResultado.SetActive(true);
        
        if (INPUT_idUsuario.text != "")
        {
           string id_ = INPUT_idUsuario.text.ToLower();
            switch (id_)
            {
                case "sabri":
                    validUser = true;
                    break;
                case "sofi":
                    validUser = true;
                    break;
                case "esteban":
                    validUser = true;
                    break;
                default:
                    validUser = false;
                    break;

            }
            
            if (validUser == true)
                ValidationResultText.text = validationTrue;
            else
                ValidationResultText.text = validationFalse;
               
        }
        else {
            ValidationResultText.text = "Es necesario que ingrese un usuario para poder validarlo";

        }
        */
    }


    private void closeResultPanel()
    {
        Debug.Log("validUser: " + validUser);
        if (validUser == true) {
           
           
            OnUserLogued();
        }
        if (validUser == false)
        {
            PanelResultado.SetActive(false);
            CleanUserInput();
        }

    }

    public void CloseLoguedPanel() {
        UI_Login.SetActive(false);
        Beneficios.SetActive(true);
    }
    public void Desloguearse()
    {
        Beneficios.SetActive(false);
        LoguedPanel.SetActive(false);
        
        UI_Login.SetActive(true);
        CleanUserInput();

    }

    private void OnUserLogued() {
        
        PanelResultado.SetActive(false);
        LoguedPanel.SetActive(true);

    }

    private void OnUserValid(bool active, string username, string useremail, string userid, string type, string fecha) {
        string activeString;
        if (active)
            activeString = "activo";
        else
            activeString = "inactivo";

        Activo.text = "Tu usuario se encuentra " + activeString;
        UserName.text = username;
        UserEmail.text = useremail;
        UserId.text = userid;
        UserType.text = type;
        FechaValida.text = fecha;
    }

    private void CleanUserInput()
    {

        INPUT_idUsuario.Select();
        INPUT_idUsuario.text = "";
    }

}
