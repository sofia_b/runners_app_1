﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DataController : MonoBehaviour {
    [Header("Services")]
    public string basicUrl = "https://tarjetarunners.com.ar";
    public string Key;// = "FDBnDScEZJ89nPeuUBUMAU0Gmnbeu6yg";
    public string UrlProducts;// = "https://tarjetarunners.marianogendra.com.ar/services/products";
    public string CategoriesURL;//= "https://tarjetarunners.marianogendra.com.ar/services/categories";
    private RunnersAppServices _services = new RunnersAppServices();
    public CatalogueParser _catalogue = new CatalogueParser();

    [Header("Categorias")]
    public int CategoriasCount;
    public List<string> ListaCategorias = new List<string>();
  

    [Header("Beneficios")]
    public int BeneficiosCount;
    public bool isCatalogueDownloaded;

   
    //public string DNI;
    private void Start()
    {
       //DownloadProducts();
     //   downloadCategories();
        //  DownloadLogin("dsd");
      // StartCoroutine(downloadUserInfo(LoginURL, "FDBnDScEZJ89nPeuUBUMAU0Gmnbeu6yg", _catalogue, "19033345"));
    }


    public int SetBeneficiosCount {

        set {
            BeneficiosCount = value;
        }
    }

    public int SetCategoriasCount
    {
        set
        {
            BeneficiosCount = value;
        }
    }

    public void DownloadProducts()
    {

        StartCoroutine(_services.downloadProducts(UrlProducts, "FDBnDScEZJ89nPeuUBUMAU0Gmnbeu6yg", _catalogue, basicUrl));
    }

    private void downloadCategories()
    {
        Debug.Log("Entra en dc");
        _services.downloadCategories(CategoriesURL, Key, _catalogue);
    }


/*
    public void DownloadLogin(string DNI)
    {
        //DNI = "37308008";
        DNI= "37008308";
       StartCoroutine(_services.downloadUserInfo(UrlProducts, "FDBnDScEZJ89nPeuUBUMAU0Gmnbeu6yg", _catalogue, DNI));
    }
*/

    public IEnumerator downloadUserInfo(string url, string key, CatalogueParser _cat, string dni)
    {
        Debug.Log("url: " + url + ", key: " + key + ", _cat: " + _cat + ", dni: " + dni);
        WWWForm form = new WWWForm();
        form.AddField("dni", dni);
        form.AddField("key", key);


        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Debug.Log("Form upload complete!");
                Debug.Log("WWDH: " + www.downloadHandler.text);
                //_cat.GeneralParse(www.downloadHandler.text);
            }
        }
    }

}
