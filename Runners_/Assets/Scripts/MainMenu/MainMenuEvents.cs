﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class MainMenuEvents : MonoBehaviour {

    public Button BTN_InactiveMenu;
    public Button BTN_ActiveMenu;
    public GameObject GO_thinMenu;
    public GameObject GO_CompleteMenu;
    public GameObject[] MenuCategories;
    public GameObject GO_Categories;

    private void Awake()
    {
        GO_thinMenu.SetActive(true);
        GO_CompleteMenu.SetActive(false);
    }


    private void Start () {
        

    }
	
	
	void Update () {
		
	}
    public void onOpenMenuCategory(string CatName) {
      
            for (int i = 0; i < MenuCategories.Length; i++)
            {
            if (MenuCategories[i].name == CatName)
            {

                MenuCategories[i].SetActive(true);
                MenuCategories[2].SetActive(true);
            }
            else
                MenuCategories[i].SetActive(false);

                  GO_CompleteMenu.SetActive(false);
                  GO_thinMenu.SetActive(true);
        }
        

    }

    public void OpenCloseMenu(string status)
    {
        if (status == "ToOpen")
        {
            Debug.Log("GO_Categories active: " + GO_Categories.gameObject.activeSelf);
             if (GO_Categories.activeSelf == false)
             {
                 GO_thinMenu.SetActive(false);
              GO_CompleteMenu.SetActive(true);
           }
        }
        if (status == "ToClose")
        {

            GO_thinMenu.SetActive(true);
            GO_CompleteMenu.SetActive(false);

        }
    }



}
