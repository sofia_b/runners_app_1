﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PopUp_CloseApp : MonoBehaviour {

    public Button Close_Yes;
    public Button Close_No;
    public GameObject GO_PopUp_CloseApp;
 
    public void OnSelectButtonClose(string decision) {
        if (decision == "yes")
            Debug.Log("cerrar app");
        if (decision == "no")
            GO_PopUp_CloseApp.SetActive(false);

    }
}
