﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RunnersAppServices : MonoBehaviour {

    private CatalogueParser cat = new CatalogueParser();
   
    public IEnumerator downloadProducts(string url, string key , CatalogueParser _cat, string urlbasic)
    {
       // Debug.Log("url: " + url + ", key: " + key + ", _cat: " + _cat);
        WWWForm form = new WWWForm();
        form.AddField("key", key);

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
             //   Debug.Log("Form upload complete!");
               Debug.Log("WWDH: " + www.downloadHandler.text);
                _cat.GeneralParse(www.downloadHandler.text, urlbasic);
            }
        }
    }

    public IEnumerator downloadCategories(string url, string key, CatalogueParser _cat)
    {
      //  Debug.Log("url: " + url + ", key: " + key + ", _cat: " + _cat);
        WWWForm form = new WWWForm();
        form.AddField("key", key);

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
           //     Debug.Log(www.error + "Categories");
            }
            else
            {
              //  Debug.Log("Form upload complete!");
                //Debug.Log("WWDH: " + www.downloadHandler.text);
                cat.CategoriesParse(www.downloadHandler.text);
            }
        }
    }

    public IEnumerator downloadUserInfo(string url, string key, string dni)
    {
        Debug.Log("url: " + url + ", key: " + key + ", _cat: " + cat + ", dni: "+ dni);
        WWWForm form = new WWWForm();
        form.AddField("dni", dni);
        form.AddField("key", key);
        

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Debug.Log("Form upload complete!");
                Debug.Log("WWDH: " + www.downloadHandler.text);
                cat.UserParse(www.downloadHandler.text);
            }
        }
    }

}
