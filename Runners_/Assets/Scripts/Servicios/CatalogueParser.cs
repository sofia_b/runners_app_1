﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;


public class CatalogueParser : MonoBehaviour {

    [Header("BeneficiosParse")]
    public List<string> ListaGeneral = new List<string>();
    public List<Beneficio> _SlotBen = new List<Beneficio>();
    public int BenefitsCount = 0;
    public UI_Beneficios UIb;
    [Header("CategoriasParse")]
    public int categoriesCount;
    public List<string> ListaCategorias = new List<string>();
    public List<string> Categorias = new List<string>();
    public Dictionary<string, string> CategoriasFilter = new Dictionary<string, string>();

    //Categories Parse

    public void CategoriesParse(string _text)
    {

     //   Debug.Log("Start to Parse Categories");

        var N = JSON.Parse(_text);
      //  BenefitsCount = N.Count;

        for (int i = 0; i < N.Count; i++)
        {
              Debug.Log(N[i].ToString());
            ListaCategorias.Add(N[i].ToString());
        }
    }


    // Products Parse

    public void GeneralParse(string _text, string basicURL) {

     //   Debug.Log("Start to Parse General");

        var N = JSON.Parse(_text);
        BenefitsCount = N.Count;
        UIb.totalBenefits = N.Count;
        for (int i = 0; i < N.Count; i++) {
          //  Debug.Log(N[i].ToString());
            ListaGeneral.Add(N[i].ToString());
        }

        BenefitsParse(ListaGeneral, basicURL);
        //GetComponent<UI_Beneficios>().SlotsCreation(N.Count, _SlotBen);
        // slotCreation(N.Count, _SlotBen);
        slotCreation(_SlotBen);


    }
    

    public void BenefitsParse( List<string> _beneficio, string basicURL)
    {
        for (int i = 0; i < BenefitsCount; i++)
        {
            SlotParse(_beneficio[i], basicURL);
            
        }
       

    }
    public bool[] aviableDays = new bool[7];
    
    public void SlotParse(string _text, string basicURL)
    {  
        //ProductoId
        Beneficio _ben = new Beneficio();
        var N = JSON.Parse(_text);
        _ben._ProductoId = N["ProductoId"];
        _ben._ProductoCategoriaId = N["ProductoCategoriaId"];
        _ben._ProductoCategoria = N["ProductoCategoria"];
        _ben._Titulo = N["Titulo"];
        _ben._DescripcionCorta = N["DescripcionCorta"];
        _ben._DescripcionCompleta = N["DescripcionCompleta"];
        _ben._Activo = N["Activo"];
        _ben._Destacado = N["Destacado"];
        _ben._VigenciaDesde = N["VigenciaDesde"];
        _ben._VigenciaHasta = N["VigenciaHasta"];       
        _ben._disponibilidadDias = N["DisponibilidadDias"];   
        _ben._tieneDescuento = N["EsPorcentaje"].AsBool;
        _ben._valorDescuento = N["Valor"];       
        _ben._moneda = N["Moneda"];
        _ben._categoriaId = N["ProductoCategoriaId"];
        _ben._categoriaName = N["ProductoCategoria"];
   
       

        

        if (!CategoriasFilter.ContainsKey(_ben._categoriaId))
        {
        //  Debug.Log("Not Contained: " + _ben._categoriaId );
            CategoriasFilter.Add(_ben._categoriaId, _ben._categoriaName);
            categoriesCount++;
           // Debug.Log("Categories count: "+ categoriesCount);
        }
        Debug.Log("basic url: " + basicURL);
        Debug.Log("IMAGEN: " + N["Imagen"]);       
        string ImageUrl = basicURL + N["Imagen"];
        Debug.Log("ImageUrl: " + ImageUrl);
        _ben._ImagenUrl = ImageUrl;
        SetAviableDays(_ben._disponibilidadDias, _ben.ListaDias);
        _SlotBen.Add(_ben);
        

       
    }

    public void slotCreation(List<Beneficio> _slotBene) {
        UIb.SlotsCreation( _slotBene);
    }
    private void SetAviableDays(string days, List<string> dias) {


      
        foreach (string d in days.Split(','))
        {
            dias.Add(d);
        }
    }


    // User Information Parse

    public void UserParse(string userInfo) {
        Debug.Log("User Info Parse");
        UserLoguin user = new UserLoguin();
        var N = JSON.Parse(userInfo);

        user.SuscripcionID = N["SuscripcionId"];
        user.NroReferencia = N["NroReferencia"];
        user.Nombre = N["Nombre"];
        user.Email = N["Email"];
        user.Tipo = N["Tipo"];
        user.Moneda = N["Moneda"];
        user.Importe = N["Importe"];
        user.FechaInicio = N["FechaInicio"];
        user.FechaFin = N["FechaFin"];
        user.EstaActivo = N["Activa"];
        user.DNI = N["Identificacion"];
        user.Telefono = N["Telefono"];
       

    }

}
