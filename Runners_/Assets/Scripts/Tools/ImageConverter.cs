﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

public class ImageConverter : MonoBehaviour {

    private Sprite SpriteTo;
   // public string UrlToTest;

    public void Start()
    {
       // StartCoroutine(ConvertUrlToImage(UrlToTest, ImageToTest));
        
    }

    public void DownloadAndSetSprite(string url, Image img)
    {
      
        StartCoroutine(ConvertUrlToImage(url, img));

    }

    private IEnumerator ConvertUrlToImage(string url, Image img) {
       
        Texture2D tex;
            tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
            using (WWW www = new WWW(url))
            {
                yield return www;
                www.LoadImageIntoTexture(tex);
                Rect rec = new Rect(0, 0, tex.width, tex.height);
                Sprite sprite=  Sprite.Create(tex, rec, new Vector2(0, 0), 1);
                img.sprite = sprite;
               
            }
    }
   
    public Sprite GetLogoSprite { get { return SpriteTo; } }
}
