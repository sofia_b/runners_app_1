﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Beneficio : MonoBehaviour {

    public Image Logo;
    public Sprite LogoSprite;
    public Text Title;
    public Text Discount;
    public Text Description;
    public Text Date;
    public Image[] WeekDaysImages;


    [Header("BeneficiosParse")]
    public int _ProductoId;//
    public int _ProductoCategoriaId;//

    public string _ImagenUrl;//  
    public string _ProductoCategoria;//
    public string _Titulo;//
    public string _DescripcionCorta;//
    public string _DescripcionCompleta;//  

    public bool _Activo;//
    public bool _Destacado;//

    public string _VigenciaDesde;//
    public string _VigenciaHasta;//
    public string _disponibilidadDias;

    public string _valorDescuento;
    public bool _tieneDescuento;
    public string _moneda;
    public string _categoriaId;
    public string _categoriaName;
   
    private bool AplicaDisponibilidad;
  
    private bool AplicaVigencia;
    private string UsuarioIngreso;
    private string UsuarioModificacion;
    private string Ubicacion;
    private string Condiciones;
    private string _FechaIngreso;

    public List<string> ListaDias = new List<string>();
    public bool[] _daysBool = new bool[7];

   
    public void setBenefit(Beneficio ben)
    {
       
        Title.text = ben._Titulo;
        Description.text = ben._DescripcionCorta;
        Date.text = "Desde " + ben._VigenciaDesde + " Hasta " + ben._VigenciaHasta;
        
        if (ben._tieneDescuento)
        {
            Discount.text = ben._valorDescuento.Substring(0, ben._valorDescuento.Length - 3) + "%";            
        }

        setBoolDays(ben.ListaDias);
       
    }

    
   
    private void setBoolDays(List<string> lista) {

        int indexDays = 0;
        foreach (string d in lista)
        {
            indexDays++;

            switch (d) {
                case "Lu":
                    _daysBool[0] = true;
                    break;
                case "Ma":
                    _daysBool[1] = true;
                    break;
                case "Mi":
                    _daysBool[2] = true;
                    break;
                case "Ju":
                    _daysBool[3] = true;
                    break;
                case "Vi":
                    _daysBool[4] = true;
                    break;
                case "Sa":
                    _daysBool[5] = true;
                    break;
                case "Do":
                    _daysBool[6] = true;
                    break;
            }

        }
        setDaysOfTheWeek(WeekDaysImages, _daysBool);

    }

    private void setDaysOfTheWeek(Image[] daysImage, bool[] days)
    {
        for (int i = 0; i < daysImage.Length; i++)
        {
           // Debug.Log("day" + i + ": " + days[i]);
            if (days[i] == false)
            {
                daysImage[i].color = new Color(0, 0, 0, 255);
            }
        }
    }

}
