﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

using UnityEngine.Networking;

public class UI_Beneficios : MonoBehaviour {

    [Header("GameObjects")]
    public GameObject BeneficiosParent;
    public GameObject BeneficiosPrefab;
    public Sprite GenericLogoToTest;

    [Header("Services")] //https://tarjetarunners.com.ar/services/products/key/FDBnDscEZJ89nPeuUBUMAU0Gmnbeu6yg
    public string basicUrl = "https://tarjetarunners.com.ar";
    public string Key = "FDBnDScEZJ89nPeuUBUMAU0Gmnbeu6yg";
    public string UrlProducts = "https://tarjetarunners.com.ar/services/products";
    public string CategoriesURL = "https://tarjetarunners.com.ar/services/categories";
    
    private RunnersAppServices _services = new RunnersAppServices();
    public ImageConverter imgConverter;
    
    public CatalogueParser _catalogue;

    public int totalBenefits;
    public List<Beneficio> SlotBeneficios = new List<Beneficio>();
    public DataController _data;

    void Start () {
       
        if (_data.isCatalogueDownloaded == false)
        {
            ParseIt();
        }
        else {
            CreateSlots();
        }
       //
    }
    
    public void ParseIt() {
        _data.isCatalogueDownloaded = true;
        StartCoroutine(_services.downloadProducts(UrlProducts, "FDBnDScEZJ89nPeuUBUMAU0Gmnbeu6yg", _catalogue, basicUrl));
    
    }

    public void CreateSlots() {
        SlotBeneficios = _catalogue._SlotBen;
        SlotsCreation(SlotBeneficios);
    }
   
    public void SlotsCreation( List<Beneficio> slotList) {
        ClearBenefits(BeneficiosParent);
        // for (int i = 0; i < numBen; i++)
        int i = 0;
       foreach(Beneficio ben in slotList)
        {                        
            GameObject go = GameObject.Instantiate(BeneficiosPrefab, BeneficiosParent.transform);
            go.name = "Beneficio - " +slotList[i]._Titulo;
            Beneficio bene = go.GetComponent<Beneficio>();
            bene._ImagenUrl = slotList[i]._ImagenUrl;
            setLogoImage(bene._ImagenUrl, bene.Logo);           
            setLogoImage(bene._ImagenUrl, bene.Logo);           
            bene._categoriaName = slotList[i]._categoriaName;
            bene._categoriaId = slotList[i]._categoriaId;
            bene._Destacado = slotList[i]._Destacado;
            bene.setBenefit(slotList[i]);           
            i++;
        }
    }

    public void setLogoImage(string urlImage, Image img)
    {
       imgConverter.DownloadAndSetSprite(urlImage, img);
    }

  
    public void VerDestacados() {

        Transform[] ts = BeneficiosParent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in ts)
        {
            if (t.GetComponent<Beneficio>() == true)
            {
                Beneficio ben = t.GetComponent<Beneficio>();
                if (ben._Destacado == true)
                    ben.gameObject.SetActive(true);
                else
                    ben.gameObject.SetActive(false);
            }
        }
        
    }
    public void verTodos()
    {
        Transform[] ts = BeneficiosParent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in ts)
        {
            if (t.GetComponent<Beneficio>() == true)
            {
                Beneficio ben = t.GetComponent<Beneficio>();
                 ben.gameObject.SetActive(true);               
            }
        }
    }

    private void ClearBenefits(GameObject parentToClean)
    {
     //   Debug.Log("ClearBenefits - parent: " + parentToClean);
        foreach (GameObject go in parentToClean.transform)
        {
            Debug.Log("Destroying: " + go);


              Destroy(go);
        }
    }
}
