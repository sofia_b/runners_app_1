﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class UI_Contacto : MonoBehaviour {


    public InputField Nombre;
    public InputField SEmail;
    public InputField Telefono;
    public InputField Asunto;
    public InputField Mensaje;
    public GameObject PopUpSendedEmail;
    
   

    public void SendEmail() {

        SendEmail(Nombre.text.ToString(),SEmail.text.ToString(), Telefono.text.ToString(), Asunto.text.ToString(), Mensaje.text.ToString());

        OnMailSended();

    }

    public void SendEmail(string SenderName, string SenderEmail, string phone, string Subject, string message)
    {
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(SenderEmail);
        mail.To.Add("speedacidrain@gmail.com");
        mail.Subject = Subject;
        mail.Body = "Nombre: " + SenderName + "\n" +"Email: " + SenderEmail + "\n" + "Telefono: " + phone + " \n" + message;

        SmtpClient smtp = new SmtpClient("smtp.gmail.com");
        smtp.Port = 587;
        smtp.Credentials = new System.Net.NetworkCredential("speedacidrain@gmail.com", "sunscape456") as ICredentialsByHost;
        smtp.EnableSsl = true;

        ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
                    return true;
                };
        smtp.Send(mail);
    }
    private void OnMailSended() {
        PopUpSendedEmail.SetActive(true);
    }
    public void CloseWindow() {
        PopUpSendedEmail.SetActive(false);
        ClearInputFields();
        this.gameObject.SetActive(false);
    }
    private void ClearInputFields() {
        Nombre.Select();
        Nombre.text = "";

        SEmail.Select();
        SEmail.text = "";

        Telefono.Select();
        Telefono.text = "";

        Asunto.Select();
        Asunto.text = "";

        Mensaje.Select();
        Mensaje.text = "";
    }
}
