﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_Categorias : MonoBehaviour {
    [Header("DataController")]
    public DataController _data;
    [Header("Services")]
    public string Key = "FDBnDScEZJ89nPeuUBUMAU0Gmnbeu6yg";
    public string UrlProducts = "https://tarjetarunners.marianogendra.com.ar/services/products";

    [Header("Categorias")]
    public List<string> CategoriesToSelect;
    public GameObject filterParent;  
    public GameObject filterPrefab;
    public Button filtrarCategorias;
    public CatalogueParser cat;
    public int cantidadFiltros;

    [Header("Beneficios")]
    public UI_Beneficios UI_beneficios;
    public GameObject ContainerBeneficios;

    private void Awake()
    {
        cantidadFiltros= cat.CategoriasFilter.Count;
    }
    void Start () {

    
       ClearFilters(filterParent);
        
        createFilters();
    }


   
    private void createFilters()//int numFilter)
    {
        ClearFilters(filterParent);
       
        Debug.Log("Categories count: " + cat.categoriesCount);
        Debug.Log("cp.CategoriasFilter: " + cat.CategoriasFilter.Count);

        foreach (KeyValuePair<string, string> kvp in cat.CategoriasFilter)
        {
            GameObject go = GameObject.Instantiate(filterPrefab, filterParent.transform);
            go.name = kvp.Key + kvp.Value;
            Filter filter = go.GetComponent<Filter>();
            filter.filterText.text = kvp.Value;


        }
    }

    private void ClearFilters(GameObject parentToClean) {
        foreach (GameObject go in parentToClean.transform) {
            Destroy(go);
        }
    }

    public void OnFilteringCategories() {
    
        CreateSlotsByFiltering(ContainerBeneficios.transform);
    

    }

    GameObject[] gos;

    public void CreateSlotsByFiltering(Transform parent)//, List<Beneficio> SlotBeneficios)
    {
        Transform[] ts = parent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in ts)
        {
           
            if (t.GetComponent<Beneficio>() == true) { 
                Beneficio ben = t.GetComponent<Beneficio>();
                //  Debug.Log(ben.name);
                // Debug.Log("Categoria: " + ben._categoriaName);
               // Debug.Log("Categoria: " + ben._categoriaName);
                if (CategoriesToSelect.Contains(ben._categoriaName))
                {
                    Debug.Log("Categoria: " + ben._categoriaName);
                    Debug.Log("Filtrada: " + ben.name);
                    ben.gameObject.SetActive(true);
                }
                else {
                    ben.gameObject.SetActive(false);
                }

            }
        }
        this.gameObject.SetActive(false);
    }
   
}
