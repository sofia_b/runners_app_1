﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Filter : MonoBehaviour {
    public Image filterImage;
    public Text filterText;
    public Color SelectedFilter;
    private bool OnClickedImage = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickFilter() {
        Debug.Log("Initial OnClickedImage: " + OnClickedImage);

        if (OnClickedImage == false) { 
            
            filterImage.color = SelectedFilter;
            filterText.color = SelectedFilter;
            AddCategorie(filterText.text);
        }

        if (OnClickedImage == true)
        {
            filterImage.color = new Color(255, 255, 255, 255);
            filterText.color = new Color(255, 255, 255, 255);
            RemoveCategorie(filterText.text);
        }
        OnClickedImage = !OnClickedImage;
      //  Debug.Log("Final OnClickedImage: " + OnClickedImage);
    }

    private void AddCategorie(string cat) {

        GameObject UI_Categories = GameObject.Find("UI_Categorias");
        List<string> ToSelect = UI_Categories.GetComponent<UI_Categorias>().CategoriesToSelect;
        if (!ToSelect.Contains(cat))
        {            
            ToSelect.Add(cat);
            Debug.Log("Categorie added: " + cat);
        }
    }
    private void RemoveCategorie(string cat)
    {

        GameObject UI_Categories = GameObject.Find("UI_Categorias");
        List<string> ToSelect = UI_Categories.GetComponent<UI_Categorias>().CategoriesToSelect;
        if (ToSelect.Contains(cat))
        {
            ToSelect.Remove(cat);
            Debug.Log("Categorie Removed: " + cat);
        }
    }
}
