﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UI_Splash : MonoBehaviour {

    public Image splash;
   
    void Start () {


         StartCoroutine(FadeInFadeOut());
      
    }
    
    private IEnumerator FadeInFadeOut() {

        StartCoroutine(FadeImage(false));
        yield return new WaitForSeconds(4);
        StartCoroutine(FadeImage(true));
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene("MainMenu");


    }

    IEnumerator FadeImage(bool fadeAway)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= Time.deltaTime/1.5f)
            {
                // set color with i as alpha
                splash.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime / 1.5f)
            {
                // set color with i as alpha
                splash.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }
}
